function particles = fastslam(landmarks, data, showGui)

% numSteps = size(trueTrajectory, 2);

% % build the landmarks
% numSensors = size(dataVA, 1);
% landmarks = struct;
% landmarkId = 0;
% for sensor = 1:numSensors
%     for anchorId = 1:size(dataVA{sensor, 1}.positions, 2)
%         landmarkId = landmarkId + 1;
%         landmarks(landmarkId).id = landmarkId;
%         landmarks(landmarkId).x = dataVA{sensor, 1}.positions(1, anchorId);
%         landmarks(landmarkId).y = dataVA{sensor, 1}.positions(2, anchorId);
%     end
% end

% % generate clutterd measurements
% % generate train measurements
% trainParameters = genTrainParameters;
% trainParameters.trainMeanNumberOfClutter = 0;
% measurements = generateTrainMeasurements(trueTrajectory, dataVA, trainParameters);
% 
% % generate train cluttered measurements
% clutteredMeasurements = generateTrainClutteredMeasurements(trueTrajectory, measurements, trainParameters);

% % build the timestep
% data = struct;
% for step = 1:numSteps
%     anchorIdx = 0;
%     for sensor = 1:numSensors
%         measurement = clutteredMeasurements{step, sensor};
%         for mea = 1:size(measurement, 2)
%             anchorIdx = anchorIdx + 1;
%             tempSensor.id = anchorIdx;
%             tempSensor.range = measurement(1, mea);
%             tempSensor.bearing = measurement(6, mea); 
%             data.timestep(step).sensor(anchorIdx) = tempSensor;
%         end
%     end
%     % temporary make odometry as zeros
%     tempOdometry.r1 = 0;
%     tempOdometry.t = 0;
%     tempOdometry.r2 = 0;
%     data.timestep(step).odometry = tempOdometry;
% end

N = size(landmarks, 2);

noise = [0.005, 0.01, 0.005]';

% how many particles
numParticles = 100;

% initialize the particles array
particles = struct;
for i = 1:numParticles
  particles(i).weight = 1. / numParticles;
  particles(i).pose = zeros(3,1);
  particles(i).history = {};
  for l = 1:N % initialize the landmarks aka the map
    particles(i).landmarks(l).observed = false;
    particles(i).landmarks(l).mu = zeros(2,1);    % 2D position of the landmark
    particles(i).landmarks(l).sigma = zeros(2,2); % covariance of the landmark
  end
end

% % toogle the visualization type
% %showGui = true;  % show a window while the algorithm runs
% showGui = false; % plot to files instead

% Perform filter update for each odometry-observation pair read from the
% data file.
if showGui == false
    [status, ~, ~] = rmdir('plots');
    if status == 1
        mkdir('plots');    
    else
        rmdir('plots', 's');
        mkdir('plots');
    end
end
    
for t = 1:size(data.timestep, 2)
%for t = 1:50
    fprintf('timestep = %d\n', t);

    % Perform the prediction step of the particle filter
    particles = prediction_step(particles, data.timestep(t).odometry, noise);

    % Perform the correction step of the particle filter
    particles = correction_step(particles, data.timestep(t).sensor);

    % Generate visualization plots of the current state of the filter
    plot_state(particles, landmarks, t, data.timestep(t).sensor, showGui);

    % Resample the particle set
    particles = resample(particles);
end

end