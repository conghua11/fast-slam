function parameters = genTrainParameters
%% train parameters
%  we can get a good result when MeasurementVariance = 1e-1^2;
%     the parameter are trainUpdateMeasurementVariance = 1e-1^2;
%     parameters trainAnchorRegularNoiseVariance = 1e-2^2 or 1e-3^2;

%  we can get a good result when MeasurementVariance = 1e-2^2;
%     the parameter are trainUpdateMeasurementVariance = 1e-2^2;
%     parameters trainAnchorRegularNoiseVariance 1e-3^2;

% filter settings
parameters.trainNumSteps = 5000;
parameters.trainNumSensors = 2;
parameters.trainNumParticles = 10000;
parameters.trainVAParticlesLimit = 500000;
parameters.trainUpSamplingFactor = 1;
parameters.trainUnreliabilityThreshold = 1e-4;

% predict
parameters.trainSurvivalProbability = 0.95;
parameters.trainAnchorRegularNoiseVariance = 1e-3^2;

% measurement
parameters.trainMeasurementVariance = 1e-3^2;    
parameters.trainMeasurementVarianceLHF = 0.15^2;
parameters.trainDetectionProbability = 0.95;
parameters.trainMeanNumberOfClutter = 1;
parameters.trainRegionOfInterestSize = 30;
parameters.trainMeanNumberOfUndetectedAnchors = 6;
parameters.trainUndetectedAnchorsIntensity = parameters.trainMeanNumberOfUndetectedAnchors/(2*parameters.trainRegionOfInterestSize)^2;

% udpate
parameters.trainUpdateMeasurementVariance = 1e-2^2;
parameters.trainClutterIntensity = parameters.trainMeanNumberOfClutter/parameters.trainRegionOfInterestSize;

% birth
parameters.trainNumNewBirth = 10000;
parameters.trainBirthFeatureWeight = 0.01;
parameters.trainMeanNumberOfBirth = 10^(-4);
parameters.trainBirthIntensity = parameters.trainMeanNumberOfBirth/(2*parameters.trainRegionOfInterestSize)^2;
