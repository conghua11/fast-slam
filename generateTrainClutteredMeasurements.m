
function [clutteredMeasurements] = generateTrainClutteredMeasurements(trueTrajectory, trueMeasurementsCell, parameters)
measurementVarianceRange = parameters.trainMeasurementVariance;
detectionProbability = parameters.trainDetectionProbability;
meanNumberOfClutter = parameters.trainMeanNumberOfClutter;
maxRange = parameters.trainRegionOfInterestSize;
[numSteps, numSensors] = size(trueMeasurementsCell);
clutteredMeasurements = cell(numSteps,1);

for sensor = 1:numSensors
    for step = 1:numSteps
        trueMeasurements = trueMeasurementsCell{step,sensor}; %增加噪声以后的测量值
        [~, numAnchors] = size(trueMeasurements);
        detectionIndicator = (rand(numAnchors,1) < detectionProbability);
        detectedMeasurements = squeeze(trueMeasurements(:,detectionIndicator)); %删除长度为1的维度
        
        numFalseAlarms = poissrnd(meanNumberOfClutter);
        falseAlarms = zeros(2,numFalseAlarms);
        if(~isempty(falseAlarms))
            for k = 1:numFalseAlarms
                falseAlarms(1,k) = maxRange*rand;
                angel = 2 * pi * rand;
                falseAlarms(4,k) = falseAlarms(1,k);
                falseAlarms(5,k) = 0;
                falseAlarms(2,k) = measurementVarianceRange;
                falseAlarms(3,k) = 0;
                falseAlarms(6,k) = angel;
            end
        end
        clutteredMeasurement = [falseAlarms, detectedMeasurements];
        clutteredMeasurement = clutteredMeasurement(:,randperm(numFalseAlarms+sum(detectionIndicator)));%随机排序
        
        clutteredMeasurements{step,sensor} = clutteredMeasurement;
    end
end

end

