function particles = fastslamAdaptor(trueTrajectory, dataVA, showGui)


numSteps = size(trueTrajectory, 2);
% build the landmarks
numSensors = size(dataVA, 1);
landmarks = struct;
landmarkId = 0;
for sensor = 1:numSensors
    for anchorId = 1:size(dataVA{sensor, 1}.positions, 2)
        landmarkId = landmarkId + 1;
        landmarks(landmarkId).id = landmarkId;
        landmarks(landmarkId).x = dataVA{sensor, 1}.positions(1, anchorId);
        landmarks(landmarkId).y = dataVA{sensor, 1}.positions(2, anchorId);
    end
end

% generate clutterd measurements
% generate train measurements
trainParameters = genTrainParameters;
trainParameters.trainMeanNumberOfClutter = 0;
measurements = generateTrainMeasurements(trueTrajectory, dataVA, trainParameters);

% generate train cluttered measurements
clutteredMeasurements = generateTrainClutteredMeasurements(trueTrajectory, measurements, trainParameters);

% build the timestep
data = struct;
for step = 1:numSteps
    anchorIdx = 0;
    for sensor = 1:numSensors
        measurement = clutteredMeasurements{step, sensor};
        for mea = 1:size(measurement, 2)
            anchorIdx = anchorIdx + 1;
            tempSensor.id = anchorIdx;
            tempSensor.range = measurement(1, mea);
            tempSensor.bearing = measurement(6, mea); 
            data.timestep(step).sensor(anchorIdx) = tempSensor;
        end
    end
    % temporary make odometry as zeros
    tempOdometry.r1 = 0;
    tempOdometry.t = 0;
    tempOdometry.r2 = 0;
    data.timestep(step).odometry = tempOdometry;
end

% call the fastSlam method
particles = fastslam(landmarks, data, showGui);

end