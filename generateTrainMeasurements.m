function [ measurementsCell ] = generateTrainMeasurements(targetTrajectory, dataVA, parameters)

measurementVarianceRange = parameters.trainMeasurementVariance;
testMeasurementVarianceLHF = parameters.trainMeasurementVarianceLHF;
[~, numSteps] = size(targetTrajectory);
numSensors = length(dataVA);
measurementsCell = cell(numSteps,numSensors);

for sensor = 1:numSensors
  positions = dataVA{sensor}.positions;
  visibility = dataVA{sensor}.visibility;
  for step = 1:numSteps
    k = 0;
    [~, numAnchors] = size(positions);
    measurements = zeros(2,numAnchors);
    for anchor = 1:numAnchors
      if(visibility(anchor,step))
        k = k+1;
        % the real distance
        realDistance = sqrt((positions(1,anchor) - targetTrajectory(1,step)).^2 + (positions(2,anchor) - targetTrajectory(2,step)).^2)';
        measurements(2,k) = measurementVarianceRange;
        noiseMea = sqrt(squeeze(measurements(2,k))).*randn;
        measurements(1,k) = realDistance +  noiseMea;
        measurements(2,k) = testMeasurementVarianceLHF;
        measurements(3,k) = anchor; %which PA or VA
%         measurements(4,k) = targetTrajectory(1,step) - positions(1,anchor) + sqrt(measurementVarianceRange) * randn; % x distance
%         measurements(5,k) = targetTrajectory(2,step) - positions(2,anchor) + sqrt(measurementVarianceRange) * randn; % y distance
%         measurements(6,k) = sqrt((positions(1,anchor) - targetTrajectory(1,step)).^2 + (positions(2,anchor) - targetTrajectory(2,step)).^2);
        measurements(4,k) = realDistance;
        measurements(5,k) = noiseMea;
        measurements(6,k) = atan((positions(2,anchor) - targetTrajectory(2,step)) / (positions(1,anchor) - targetTrajectory(1,step)));
      end
    end
    measurementsCell{step,sensor} = measurements(:,1:k);
  end
end

end

